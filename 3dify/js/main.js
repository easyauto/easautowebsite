// Gumby is ready to go
Gumby.ready(function() {
	Gumby.log('Gumby is ready to go...', Gumby.dump());

	// placeholder polyfil
	if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
		$('input, textarea').placeholder();
	}

	// skip link and toggle on one element
	// when the skip link completes, trigger the switch
	$('#skip-switch').on('gumby.onComplete', function() {
		$(this).trigger('gumby.trigger');
	});

// Oldie document loaded
}).oldie(function() {
	Gumby.warn("This is an oldie browser...");

// Touch devices loaded
}).touch(function() {
	Gumby.log("This is a touch enabled device...");
});
//custom jquey
$(function(){
	$height = $(window).height();
	$("#hero").height($height - 100);
	$("#about").height($height);
	//$("#about").css("margin-top",$height);

	//make nav bar sticky
	$('#about').waypoint(function(up) {
  		$('.about-title').transition({ marginLeft: '40%'}, 700, 'ease');
  		$('.about-content').transition({ marginLeft: '25%'}, 500, 'ease');
  	}, { offset: 250 });
  	//revert header from scroll back
  	$('#services').waypoint(function(up) {
  		$('.service-title').transition({ opacity: 1 }, 700, 'ease');
  		$('.service-content p').transition({ opacity: 1 }, 500, 'ease');
  		$('.service-content').transition({ opacity: 1 }, 500, 'ease');
  	}, { offset: 250 });
  	
  	$(".hoverbox")
    .mouseenter(function () {
        $(this).addClass("on");
    })
    .mouseleave(function () {
        $(this).removeClass("on");
    });

	//parallax thing code
	$window = $(window);
  	$('section[data-type="background"]').each(function(){
        var $bgobj = $(this); // assigning the object
     
        $(window).scroll(function() {
            var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
             
            // Put together our final background position
            var coords = '50% '+ yPos + 'px';
 
            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        }); 
    }); 
});
//intit skrollr
