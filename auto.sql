
-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2013 at 12:46 AM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a5410916_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `trans` bigint(20) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`trans`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` VALUES(1, 9446163763, '2013-09-25 13:29:52', 1);
INSERT INTO `customer` VALUES(2, 0, '2013-10-02 11:19:24', 0);
INSERT INTO `customer` VALUES(3, 0, '2013-10-02 21:22:12', 0);
INSERT INTO `customer` VALUES(4, 0, '2013-10-03 09:34:36', 0);
INSERT INTO `customer` VALUES(5, 8129412217, '2013-10-03 14:37:43', 0);
INSERT INTO `customer` VALUES(6, 0, '2013-10-03 22:43:21', 0);
INSERT INTO `customer` VALUES(7, 0, '2013-10-03 22:44:39', 0);
INSERT INTO `customer` VALUES(8, 0, '2013-10-03 22:50:42', 0);
INSERT INTO `customer` VALUES(9, 0, '2013-10-04 00:54:00', 0);
INSERT INTO `customer` VALUES(10, 0, '2013-10-04 00:55:44', 0);
INSERT INTO `customer` VALUES(11, 8547104763, '2013-10-04 00:57:43', 0);
INSERT INTO `customer` VALUES(12, 9567607741, '2013-10-08 11:13:32', 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `phone` bigint(20) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`phone`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` VALUES(8547104763, 76.9474, 8.48332, 'karthik');
INSERT INTO `driver` VALUES(0, 0, 0, '');
INSERT INTO `driver` VALUES(8129412922, -73.6401, 40.6387, 'name');
INSERT INTO `driver` VALUES(8129412217, -73.6401, 40.6387, 'name');
INSERT INTO `driver` VALUES(9567607741, 76.9474, 8.48332, 'rahul');
INSERT INTO `driver` VALUES(9400609385, 76.9474, 8.48332, 'jery');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `sms` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-09-25 13:18:53');
INSERT INTO `message` VALUES('AUTO trivandrum eastfort sreekaryam 9446163763', '2013-09-25 13:29:51');
INSERT INTO `message` VALUES('TRANS 1 A 8547104763', '2013-09-25 13:31:10');
INSERT INTO `message` VALUES('TRANS 1 A 8547104763', '2013-09-25 13:33:30');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-09-25 13:36:28');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-09-25 22:36:13');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 00:50:52');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 00:51:21');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 00:51:31');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 01:04:45');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 02:11:49');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 02:12:02');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 02:12:22');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 02:12:30');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 02:14:56');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 02:18:50');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 05:28:51');
INSERT INTO `message` VALUES('', '2013-10-02 06:56:05');
INSERT INTO `message` VALUES('9400540440', '2013-10-02 06:56:50');
INSERT INTO `message` VALUES('DRIVER', '2013-10-02 06:57:41');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-02 08:37:23');
INSERT INTO `message` VALUES('', '2013-10-02 08:37:40');
INSERT INTO `message` VALUES('', '2013-10-02 08:37:56');
INSERT INTO `message` VALUES('DRIVER city place name phonenumber', '2013-10-02 08:37:57');
INSERT INTO `message` VALUES('', '2013-10-02 11:17:49');
INSERT INTO `message` VALUES('', '2013-10-02 11:17:56');
INSERT INTO `message` VALUES('DRIVER', '2013-10-02 11:18:26');
INSERT INTO `message` VALUES('AUTO', '2013-10-02 11:19:23');
INSERT INTO `message` VALUES('', '2013-10-02 21:20:30');
INSERT INTO `message` VALUES('TRANS 9400540440', '2013-10-02 21:21:13');
INSERT INTO `message` VALUES('AUTO', '2013-10-02 21:22:11');
INSERT INTO `message` VALUES('TVM', '2013-10-02 21:22:24');
INSERT INTO `message` VALUES('', '2013-10-03 09:34:27');
INSERT INTO `message` VALUES('AUTO', '2013-10-03 09:34:35');
INSERT INTO `message` VALUES('DRIVER', '2013-10-03 09:34:54');
INSERT INTO `message` VALUES('DRIVERcityplacename8129412922', '2013-10-03 09:35:53');
INSERT INTO `message` VALUES('DRIVER city place name 8129412922', '2013-10-03 09:36:05');
INSERT INTO `message` VALUES('DRIVER city place name 8129412922', '2013-10-03 10:23:43');
INSERT INTO `message` VALUES('', '2013-10-03 14:29:21');
INSERT INTO `message` VALUES('DRIVER city place name 8129412217', '2013-10-03 14:29:55');
INSERT INTO `message` VALUES('AUTO city origin destination 8129412217', '2013-10-03 14:37:41');
INSERT INTO `message` VALUES('RANS transactionID A 8129412217', '2013-10-03 14:38:06');
INSERT INTO `message` VALUES('', '2013-10-03 22:38:43');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-03 22:38:59');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8547104763', '2013-10-03 22:39:09');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort rahul 9567607741', '2013-10-03 22:40:17');
INSERT INTO `message` VALUES('AUTO eastfort vellayambalam 8547104763', '2013-10-03 22:43:20');
INSERT INTO `message` VALUES('AUTO trivandrum vellayambalam 8547104763', '2013-10-03 22:44:38');
INSERT INTO `message` VALUES('AUTO eastfort vellayambalam 8547104763', '2013-10-03 22:50:41');
INSERT INTO `message` VALUES('AUTO eastfort vellayambalam 8547104763', '2013-10-04 00:53:59');
INSERT INTO `message` VALUES('AUTO trivandrum eastfort trivandrum vellayambalam 8547104763', '2013-10-04 00:55:43');
INSERT INTO `message` VALUES('AUTO trivandrum eastfort vellayambalam 8547104763', '2013-10-04 00:57:42');
INSERT INTO `message` VALUES('TRANS 11 9567607741', '2013-10-04 01:00:37');
INSERT INTO `message` VALUES('DRIVER', '2013-10-04 08:17:54');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort karthik 8129412217', '2013-10-04 08:18:31');
INSERT INTO `message` VALUES('DRIVER', '2013-10-07 14:38:10');
INSERT INTO `message` VALUES('DRIVER trivandrum eastfort jery 9400609385', '2013-10-08 11:10:38');
INSERT INTO `message` VALUES('AUTO trivandrum eastfort sreekaryam 9567607741', '2013-10-08 11:13:31');
INSERT INTO `message` VALUES('', '2013-11-23 12:31:20');
